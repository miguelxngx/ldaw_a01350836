from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return HttpResponse('<em>My Second App</em>')

def help(request):
    dict = {
        'title' : 'haaalp',
        'mainText' : 'You are on the help page',
        'helpText' : 'Try to solve the problem by yourself'
    }
    return render(request, 'AppTwo/help.html', context=dict)
# Create your views here.
